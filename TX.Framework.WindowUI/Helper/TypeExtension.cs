﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TX.Framework.WindowUI.Helper
{
	public static class TypeExtension
	{
		public static string GetDescription(this System.Enum value)
		{
			string result;
			if (Validate.IsEmpty<System.Enum>(value))
			{
				result = string.Empty;
			}
			else
			{
				string text = value.ToString();
				FieldInfo field = value.GetType().GetField(text);
				if (field == null)
				{
					result = text;
				}
				else
				{
					object obj = field.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault<object>();
					if (Validate.IsEmpty(obj))
					{
						result = text;
					}
					else
					{
						DescriptionAttribute descriptionAttribute = (DescriptionAttribute)obj;
						result = descriptionAttribute.Description;
					}
				}
			}
			return result;
		}

		public static int ToInt32(this string value)
		{
			int result;
			if (string.IsNullOrEmpty(value))
			{
				result = 0;
			}
			else
			{
				int num = 0;
				int.TryParse(value, out num);
				result = num;
			}
			return result;
		}

		public static bool ToBoolean(this string value)
		{
			bool result;
			if (bool.TryParse(value, out result))
			{
				return result;
			}
			throw new InvalidCastException("\"" + value + "\"不是有效的时间格式，请确认。");
		}

		public static string Value(this bool value)
		{
			return value ? "1" : "0";
		}

		public static Dictionary<string, string> ToDictionary(this string urlValue, char paraSplitChar = '&', char keyValueSplitChar = '=')
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			Dictionary<string, string> result;
			if (string.IsNullOrEmpty(urlValue))
			{
				result = dictionary;
			}
			else
			{
				string[] array = urlValue.Split(new char[]
				{
					paraSplitChar
				});
				for (int i = 0; i < array.Length; i++)
				{
					string[] array2 = array[i].Split(new char[]
					{
						keyValueSplitChar
					});
					if (array2.Length == 1)
					{
						dictionary.Add(array2[0], string.Empty);
					}
					else if (array2.Length >= 2)
					{
						dictionary.Add(array2[0], array[i].Substring(array2[0].Length + 1));
					}
				}
				result = dictionary;
			}
			return result;
		}

		public static string ToKeyValueString(this Dictionary<string, string> dic, bool IncludeEmpytValue = false, char paraSplitChar = '&', char keyValueSplitChar = '=')
		{
			string result;
			if (dic != null && dic.Count > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (KeyValuePair<string, string> keyValuePair in dic)
				{
					if (IncludeEmpytValue || (!string.IsNullOrEmpty(keyValuePair.Value) && !keyValuePair.Value.Equals(0)))
					{
						stringBuilder.Append(keyValuePair.Key).Append(keyValueSplitChar).Append(keyValuePair.Value).Append(paraSplitChar);
					}
				}
				string text = stringBuilder.ToString();
				result = text.Substring(0, text.Length - 1);
			}
			else
			{
				result = string.Empty;
			}
			return result;
		}

		public static string ToIntString(this decimal value)
		{
			string result;
			if ((int)value == value)
			{
				result = ((int)value).ToString();
			}
			else
			{
				result = value.ToString().TrimEnd(new char[]
				{
					'0'
				});
			}
			return result;
		}

		public static int ToCeil(this decimal value)
		{
			decimal value2 = Math.Ceiling(value);
			return (int)value2;
		}

		public static decimal ToDecimal(this string value)
		{
			decimal result;
			if (decimal.TryParse(value, out result))
			{
				return result;
			}
			throw new InvalidCastException("不能将字符串\"" + value + "\"转换为Decimal数字。");
		}

		public static decimal ToSafeDecimal(this string value)
		{
			decimal num;
			decimal result;
			if (decimal.TryParse(value, out num))
			{
				result = num;
			}
			else
			{
				result = 0m;
			}
			return result;
		}

		public static string ToDateStringFromNow(this DateTime dt)
		{
			TimeSpan timeSpan = DateTime.Now - dt;
			string result;
			if (timeSpan.TotalDays > 60.0)
			{
				result = dt.ToShortDateString();
			}
			else if (timeSpan.TotalDays > 30.0)
			{
				result = "1个月前";
			}
			else if (timeSpan.TotalDays > 14.0)
			{
				result = "2周前";
			}
			else if (timeSpan.TotalDays > 7.0)
			{
				result = "1周前";
			}
			else if (timeSpan.TotalDays > 1.0)
			{
				result = string.Format("{0}天前", (int)Math.Floor(timeSpan.TotalDays));
			}
			else if (timeSpan.TotalHours > 1.0)
			{
				result = string.Format("{0}小时前", (int)Math.Floor(timeSpan.TotalHours));
			}
			else if (timeSpan.TotalMinutes > 1.0)
			{
				result = string.Format("{0}分钟前", (int)Math.Floor(timeSpan.TotalMinutes));
			}
			else if (timeSpan.TotalSeconds >= 1.0)
			{
				result = string.Format("{0}秒前", (int)Math.Floor(timeSpan.TotalSeconds));
			}
			else if (dt.Date == DateTime.Now.Date)
			{
				result = "今天";
			}
			else
			{
				int num = (int)Math.Floor(Math.Abs((dt - DateTime.Now).TotalDays));
				if (num == 1)
				{
					result = "明天";
				}
				else if (num == 2)
				{
					result = "后天";
				}
				else
				{
					result = string.Format("{0}天后", num);
				}
			}
			return result;
		}

		public static DateTime ToDateTime(this string value, string format)
		{
			DateTime result;
			if (DateTime.TryParseExact(value, format, new CultureInfo("zh-CHS"), DateTimeStyles.None, out result))
			{
				return result;
			}
			throw new Exception(string.Format("字符串（{0}）不能按照格式（{1}）解析为时间", value, format));
		}

		public static DateTime ToDateTime(this string dateTime)
		{
			DateTime result;
			if (DateTime.TryParse(dateTime, out result))
			{
				return result;
			}
			throw new InvalidCastException("\"" + dateTime + "\"不是有效的时间格式，请确认。");
		}

		public static bool IsValid(this DateTime value)
		{
			return !(DateTime.MinValue == value) && !(DateTime.MaxValue == value);
		}

		public static short ToInt16(this string value)
		{
			short result;
			if (short.TryParse(value, out result))
			{
				return result;
			}
			throw new InvalidCastException("不能将字符串\"" + value + "\"转换为短整形数字。");
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000058B4 File Offset: 0x00003AB4
		public static long ToInt64(this string value)
		{
			long result;
			if (long.TryParse(value, out result))
			{
				return result;
			}
			throw new InvalidCastException("不能将字符串\"" + value + "\"转换为长整形数字。");
		}

		public static string ToLogString(this Exception ex)
		{
			string result;
			if (ex == null)
			{
				result = string.Empty;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("   >>错误消息（Message）：").Append(ex.Message);
				if (!string.IsNullOrEmpty(ex.StackTrace))
				{
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append("   >>堆栈信息（StackTrace）：");
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(ex.StackTrace);
				}
				if (ex.InnerException != null)
				{
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(">>=========== 内部异常（InnerException）===========");
					stringBuilder.Append(Environment.NewLine);
					stringBuilder.Append(ex.InnerException.ToLogString());
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		public static string AllMessage(this Exception ex)
		{
			string result;
			if (ex == null)
			{
				result = string.Empty;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(ex.Message);
				if (ex.InnerException != null)
				{
					stringBuilder.Append(">").Append(ex.InnerException.AllMessage());
					string[] array = stringBuilder.ToString().Split(new char[]
					{
						'>'
					});
					if (array.Length > 1)
					{
						return array[array.Length - 1];
					}
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		public static string GetEnumNameByValue<T>(this object value)
		{
			Type typeFromHandle = typeof(T);
			if (typeFromHandle.IsEnum)
			{
				return System.Enum.GetName(typeFromHandle, value);
			}
			throw new InvalidCastException("必须是枚举类型才能获取枚举名称。");
		}

		public static T ToEnumByValue<T>(this object value)
		{
			return value.GetEnumNameByValue<T>().ToEnum<T>();
		}

		public static T ToEnum<T>(this string name)
		{
			Type typeFromHandle = typeof(T);
			if (typeFromHandle.IsEnum)
			{
				return (T)((object)System.Enum.Parse(typeof(T), name));
			}
			throw new InvalidCastException("必须是枚举类型才能转换。");
		}
	}
}
